<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cliente;

class clientecontroller extends Controller
{
    public function InicioCliente(Request $request)
    {
        $cliente = cliente::all();
       return view('cliente.inicio')->with('cliente', $cliente);
    }

    public function CrearCliente(Request $request)
    {
        $cliente = cliente::all();
        return view('cliente.crear')->with('cliente', $cliente);
    }
    
    public function GuardarCliente(Request $request){
        $this->validate($request, [
            'Nombre' => 'required',
            'Apellidos'=> 'required',
            'Cedula'=> 'required',
            'Direccion'=> 'required',
            'Telefono'=> 'required',
            'Fecha_Nacimiento'=> 'required',
            'Email'=> 'required'
        ]);

        $cliente = new cliente;
        $cliente->Nombre=$request->Nombre;
        $cliente->Apellidos=$request->Apellidos;
        $cliente->Cedula=$request->Cedula;
        $cliente->Direccion=$request->Direccion;
        $cliente->Telefono=$request->Telefono;
        $cliente->Fecha_Nacimiento=$request->Fecha_Nacimiento;
        $cliente->Email=$request->Email;
        $cliente->save();
        return redirect()->route('list.clientes');
    }
}
